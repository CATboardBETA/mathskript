package personal.williamnelson.mathskript;

import java.nio.file.Path;
import java.util.concurrent.Callable;

import picocli.CommandLine;
import picocli.CommandLine.Command;


@Command(name = "MathSkript", mixinStandardHelpOptions = true,
        version = "mathskript early release beta 1",
        description = "Scripting language for all your math needs.",
        footer = {"copywrite (c) 2021 William Nelson", "Licensed under MIT"})
class MathSkript implements Callable<Integer> {

    @CommandLine.Option(names = {"-v", "--verb", "--verbpse"},
            description = "Display more information")
    boolean verbose;

    @CommandLine.Option(names = {"-i", "--in", "--input", "--infile"},
            description = "Input .mskr MathSkript file",
            type = Path.class)
    Path infilePath;

    @CommandLine.Option(names = {"-o", "--out", "--output", "--outfile"},
            description = "Output .cmskr Compiled MathSkript file",
            type = Path.class)
    Path outfilePath;

    @CommandLine.Option(names = {"-l", "--log", "--logfile"},
            description = "File to log to",
            type = Path.class)
    Path logfilePath;


    // this example implements Callable, so parsing, error handling and handling user
    // requests for usage help or version help can be done with one line of code.
    public static void main(String... args) {
        int exitCode = new CommandLine(new MathSkript()).execute(args);
        System.exit(exitCode);
    }


        public Integer call() throws Exception {
                return 0;
        }
}