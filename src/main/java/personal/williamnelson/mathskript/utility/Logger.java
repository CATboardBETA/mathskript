package personal.williamnelson.mathskript.utility;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.nio.file.Path;
import java.time.Clock;

/**
 * Simple class to log basic information to a file.
 */
public class Logger {
    
    /**
     * Internal enumeration for the different logging 
     * types. Used in the private logHeader(LogType) method.
     */
    private enum LogType {
        INFO, WARN, DEBUG, ERROR, FATAL
    }

    private FileOutputStream fos;
    private BufferedOutputStream bos;
    private DataOutputStream dos;

    public Logger(Path pathToLogTo) {
        try {
            fos = new FileOutputStream(pathToLogTo.toFile());
        } catch (FileNotFoundException e) {
            System.out.println("Logfile " + pathToLogTo.toString() + " does not exist.");
        }

        bos = new BufferedOutputStream(fos);
        dos = new DataOutputStream(bos);
    }

    public void info(String message) throws IOException {
        dos.writeUTF(logHeader(LogType.INFO) + message);
    }

    public void warn(String message) throws IOException {
        dos.writeUTF(logHeader(LogType.WARN) + message);
    }

    public void debug(String message) throws IOException {
        dos.writeUTF(logHeader(LogType.DEBUG) + message);
    }

    public void error(String message) throws IOException {
        dos.writeUTF(logHeader(LogType.ERROR) + message);
    }

    public void fatal(String message) throws IOException {
        dos.writeUTF(logHeader(LogType.FATAL) + message);
    }

    private String logHeader(LogType type) {
        return "[" + Clock.systemUTC().instant().toString() + "] : [" + type + "] : ";
    }
}
